package com.jsilva.clima_app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    TextView nombreCiudad;
    Button searchButton;
    TextView result;

    class Weather extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... address) {
            //String... significa que se pueden enviar múltiples direcciones. Actúa como un Arreglo.

         try {
             URL url = new URL(address[0]);
             HttpURLConnection connection = (HttpURLConnection) url.openConnection();
             //Establece conexión con la dirección.
             connection.connect();

             //Recuperar datos desde la url
             InputStream is = connection.getInputStream();
             InputStreamReader isr = new InputStreamReader(is);

             //Recuperar datos y devolver como String
             int data = isr.read();
             String content = "";
             char ch;
             while (data != -1){
                 ch = (char) data;
                 content = content + ch;
                 data = isr.read();

             }
             return content;


         } catch (MalformedURLException e) {
             e.printStackTrace();

         } catch (IOException e){
             e.printStackTrace();
         }


            return null;
        }
    }

    public void buscar (View view){
        nombreCiudad = findViewById(R.id.nombreCiudad);
        searchButton = findViewById(R.id.searchButton);
        result = findViewById(R.id.result);


        String nCiudad = nombreCiudad.getText().toString();

        String content;
        Weather weather = new Weather();
        try {
            content = weather.execute("http://openweathermap.org/data/2.5/weather?q=" +
                    nCiudad+"&appid=b6907d289e10d714a6e88b30761fae22").get();
            Log.i("content",content);

            //JSON
            JSONObject jsonObject = new JSONObject(content);
            String weatherData = jsonObject.getString("weather");
            String mainTemperature = jsonObject.getString("main");
            double visibility;


            //Log.i("weatherData",weatherData);
            //weatherData esta en un Array
            JSONArray array = new JSONArray(weatherData);

            String main = "";
            String description = "";
            String temperature = "";

            for (int i=0; i<array.length(); i++){
                JSONObject weatherPart = array.getJSONObject(i);
                main = weatherPart.getString("main");
                description = weatherPart.getString("description");
            }

            JSONObject mainPart = new JSONObject(mainTemperature);
            temperature = mainPart.getString("temp");
            visibility = Double.parseDouble(jsonObject.getString("visibility"));
            int visibilityInKilometer = (int) visibility/1000;

            Log.i("Temperature",temperature);

            /*Log.i("main",main);
            Log.i("description",description);*/

            String resultText = "Condición Actual : "+main+
                    "\nDetalle : "+description+
                    "\nTemperatura : "+temperature+" ºC"+
                    "\nVisibilidad :"+visibilityInKilometer+" Km";

            result.setText(resultText);





        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



    }
}
